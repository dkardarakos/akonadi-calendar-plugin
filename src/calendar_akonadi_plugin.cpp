/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "calendar_akonadi_plugin.h"

#include "collectioncalendar.h"
#include <QFile>
#include <QDebug>
#include <QMap>
#include <KCalendarCore/Event>
#include <KPluginFactory>
#include <KCalendarCore/MemoryCalendar>
#include <AkonadiCore/CollectionFetchJob>
#include <Akonadi/Calendar/ETMCalendar>
#include <AkonadiCore/EntityTreeModel>
#include <AkonadiCore/Monitor>
#include <AkonadiCore/Control>
#include <KCheckableProxyModel>
#include <Akonadi/Calendar/IncidenceChanger>

K_PLUGIN_CLASS_WITH_JSON(CalendarAkonadiPlugin, "calendar_akonadi_plugin.json")

class CalendarAkonadiPlugin::Private
{
public:
    Private(CalendarAkonadiPlugin *parentPlugin)
        : q {parentPlugin}, m_collections {}
    {};

    void setupCalendars();
    void setupCalendar(const Akonadi::Collection::Id collectionId);
    void enableCollection(const Akonadi::Collection::Id collectionId, KCheckableProxyModel *model, const QModelIndex &parent = QModelIndex());

    CalendarAkonadiPlugin *q;
    QMap<Akonadi::Collection::Id, QString> m_collections;
    QMap<Akonadi::Collection::Id, CollectionCalendar::Ptr> m_collection_calendars;
};

void CalendarAkonadiPlugin::Private::setupCalendars()
{
    const auto collectionKeys = m_collections.keys();
    for (const auto &cKey : collectionKeys) {
        const QStringList mimeTypes {KCalendarCore::Event::eventMimeType(), KCalendarCore::Todo::todoMimeType()};
        auto etmCalendar = CollectionCalendar::Ptr {new CollectionCalendar {cKey, mimeTypes}};
        etmCalendar->setId(QString("akonadi-caldav-%1").arg(cKey));
        etmCalendar->setName(m_collections[cKey]);
        m_collection_calendars[cKey] = etmCalendar;

        connect(etmCalendar->entityTreeModel(), &Akonadi::EntityTreeModel::collectionTreeFetched, [this, cKey]() {
            setupCalendar(cKey);
        });

        connect(etmCalendar.data(), &Akonadi::ETMCalendar::calendarChanged, [this]() {
            qDebug() << "etmCalendar changed";
            Q_EMIT q->calendarsChanged();
        });
    }
}

void CalendarAkonadiPlugin::Private::setupCalendar(const Akonadi::Collection::Id collectionId)
{
    auto treeModel = m_collection_calendars[collectionId]->entityTreeModel();
    if (!treeModel->isCollectionTreeFetched()) {
        return;
    }

    KCheckableProxyModel *checkableModel =  m_collection_calendars[collectionId]->checkableProxyModel();
    enableCollection(collectionId, checkableModel);
    Q_EMIT q->calendarsChanged();
}

void CalendarAkonadiPlugin::Private::enableCollection(const Akonadi::Collection::Id collectionId, KCheckableProxyModel *model, const QModelIndex &parent)
{
    const int rowCount = model->rowCount(parent);

    for (int row = 0; row < rowCount; ++row) {
        QModelIndex index = model->index(row, 0, parent);
        Akonadi::Collection col = index.data(Akonadi::EntityTreeModel::CollectionRole).value<Akonadi::Collection>();
        if (model->rowCount(index) == 0) {
            if ((col.id() == collectionId)) {
                model->setData(index, Qt::Checked, Qt::CheckStateRole);
                m_collection_calendars[collectionId]->entityTreeModel()->setCollectionMonitored(col, true);
                m_collection_calendars[collectionId]->incidenceChanger()->setDefaultCollection(col);
            }
        }

        else {
            enableCollection(collectionId, model, index);
        }
    }
}

CalendarAkonadiPlugin::CalendarAkonadiPlugin(QObject *parent, const QVariantList &args)
    : KCalendarCore::CalendarPlugin(parent, args), d { new Private {this} }
{
    if (!Akonadi::Control::start()) {
        qDebug() << "CalendarAkonadiPlugin: Unable to start Akonadi server, exit application";
        return;
    }

    auto *job = new Akonadi::CollectionFetchJob {Akonadi::Collection::root(), Akonadi::CollectionFetchJob::Recursive};
    connect(job, &Akonadi::CollectionFetchJob::collectionsReceived, [this](Akonadi::Collection::List collectionsList) {
        for (auto &collection : collectionsList) {
            const auto mt = collection.contentMimeTypes();
            if (mt.contains("application/x-vnd.akonadi.calendar.todo") && mt.contains("application/x-vnd.akonadi.calendar.event") && mt.contains("inode/directory")) {
                qDebug() << "CalendarAkonadiPlugin: insert collection" << collection.id() << collection.name() << collection.displayName();
                d->m_collections.insert(collection.id(), collection.displayName());

            }
        }
        d->setupCalendars();
    });
}

QVector<KCalendarCore::Calendar::Ptr> CalendarAkonadiPlugin::calendars() const
{
    QVector<KCalendarCore::Calendar::Ptr> cals {};

    for (auto &c : d->m_collection_calendars) {
        cals.push_back(c);
    }

    return cals;
}

#include "calendar_akonadi_plugin.moc"

