/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "collectioncalendar.h"
#include <AkonadiCore/LinkJob>
#include <AkonadiCore/Item>
#include <AkonadiCore/ItemCreateJob>
#include <Akonadi/Calendar/IncidenceChanger>

#include <KJob>

class CollectionCalendar::Private
{
public:
    Private()
        : m_collection_id {-1}
    {};

    Akonadi::Collection::Id m_collection_id;
};

CollectionCalendar::CollectionCalendar(const Akonadi::Collection::Id collectionId, Akonadi::ETMCalendar *calendar, QObject *parent) : Akonadi::ETMCalendar(calendar, parent), d {new Private}
{
    d->m_collection_id = collectionId;
}

CollectionCalendar::CollectionCalendar(const Akonadi::Collection::Id collectionId, const QStringList &mimeTypes, QObject *parent) : Akonadi::ETMCalendar(mimeTypes, parent), d {new Private}
{
    qDebug() << "create calendar for collection " << collectionId;
    d->m_collection_id = collectionId;
}

CollectionCalendar::CollectionCalendar(const Akonadi::Collection::Id collectionId, QObject *parent) : Akonadi::ETMCalendar(parent), d {new Private}
{
    d->m_collection_id = collectionId;
}

CollectionCalendar::~CollectionCalendar()
{
    close();
}

bool CollectionCalendar::beginChange(const KCalendarCore::Incidence::Ptr &incidence)
{
    Q_UNUSED(incidence);

    return true;
}

bool CollectionCalendar::endChange(const KCalendarCore::Incidence::Ptr &incidence)
{
    auto i = this->incidence(incidence->uid());
    if (i != nullptr) {
        qDebug() << i->uid() << "modified";
        return this->modifyIncidence(i);
    }
    return false;
}

bool CollectionCalendar::addEvent(const KCalendarCore::Event::Ptr &event)
{
    return this->incidenceChanger()->createIncidence(event, this->incidenceChanger()->defaultCollection());
}

bool CollectionCalendar::addTodo(const KCalendarCore::Todo::Ptr &todo)
{
    return this->incidenceChanger()->createIncidence(todo, this->incidenceChanger()->defaultCollection());
}

