/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <KCalendarCore/Calendar>
#include <KCalendarCore/CalendarPlugin>

#ifndef CALENDAR_AKONADI_PLUGIN_H
#define CALENDAR_AKONADI_PLUGIN_H

class CalendarAkonadiPlugin : public KCalendarCore::CalendarPlugin
{
    Q_OBJECT
public:
    CalendarAkonadiPlugin(QObject *parent, const QVariantList &args);
    QVector<KCalendarCore::Calendar::Ptr> calendars() const override;

    class Private;
    Private *d;
};
#endif // CALENDAR_AKONADI_PLUGIN_H
