/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef COLLECTION_CALENDAR_H
#define COLLECTION_CALENDAR_H

#include <QTimeZone>
#include <QByteArray>
#include <Akonadi/Calendar/ETMCalendar>
#include <KCalendarCore/Incidence>

class CollectionCalendar : public Akonadi::ETMCalendar
{
    Q_OBJECT
public:
    /**
      A shared pointer to a CollectionCalendar
    */
    typedef QSharedPointer<CollectionCalendar> Ptr;

    explicit CollectionCalendar(const Akonadi::Collection::Id collectionId, QObject *parent = nullptr);
    explicit CollectionCalendar(const Akonadi::Collection::Id collectionId, const QStringList &mimeTypes, QObject *parent = nullptr);
    explicit CollectionCalendar(const Akonadi::Collection::Id collectionId, Akonadi::ETMCalendar *calendar, QObject *parent = nullptr);

    ~CollectionCalendar() override;

    virtual bool endChange(const KCalendarCore::Incidence::Ptr   &incidence) override;
    virtual bool beginChange(const KCalendarCore::Incidence::Ptr   &incidence) override;
    virtual bool addEvent(const KCalendarCore::Event::Ptr &event) override;
    virtual bool addTodo(const KCalendarCore::Todo::Ptr &todo) override;

private:
    class Private;
    Private *d;
};
#endif // COLLECTION_CALENDAR_H
